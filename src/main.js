import Vue from "vue";
import Buefy from "buefy";
import VuePersist from "vue-persist";
import "buefy/dist/buefy.css";
import VueClipboard from "vue-clipboard2";
import App from "./App.vue";
import router from "./router";

Vue.use(VueClipboard);
Vue.use(VuePersist);
Vue.use(Buefy);
Vue.prototype.$vue = Vue;
Vue.prototype.$weburl = "https://rexolt.site";
Vue.prototype.$scrape_url =
  process.env.NODE_ENV === "development"
    ? "http://157.230.137.163:9080/crawl.json?start_requests=true&spider_name=leak_scraper"
    : "https://rexolt.site/api/getleaks";

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
