import Vue from "vue";
import Router from "vue-router";
import SongsTab from "./views/tabs/SongsTab";

Vue.use(Router);

const router = new Router({
  linkActiveClass: "is-active",
  linkExactActiveClass: "is-active",
  mode: "history",
  routes: [
    {
      path: "/",
      name: "SongsTab",
      component: SongsTab
    },
    {
      path: "/snippets",
      name: "SnippetsTab",
      component: () => import(/* webpackPrefetch: true */ "./views/tabs/SnippetsTab")
    },
    {
      path: "/trackers",
      name: "TrackersTab",
      component: () => import("./views/tabs/TrackersTab")
    },
    {
      path: "/legal",
      name: "legal",
      component: () => import("./views/Legal")
    },
    {
      path: "/blog",
      name: "blog",
      component: () => import("./views/Blog")
    },
    {
      path: "/faq",
      name: "faq",
      component: () => import("./views/Faq")
    },
    {
      path: "/favorites",
      name: "favorites",
      component: () => import(/* webpackPrefetch: true */ "./views/Favorites")
    },
    {
      path: "/item/:id",
      name: "item",
      component: () => import("./views/Item"),
      props: true
    },
    {
      path: "/report/:id",
      name: "report",
      component: () => import("./views/Report")
    },
    {
      path: "/cms/",
      name: "cms-index",
      component: () => import("./views/cms/Index")
    },
    {
      path: "/cms/song",
      name: "cms-song-index",
      component: () => import("./views/cms/song/Index")
    },
    {
      path: "/cms/song/edit/:id",
      name: "cms-song-edit",
      component: () => import("./views/cms/song/EditSong")
    },
    {
      path: "/cms/song/add",
      name: "cms-song-add",
      component: () => import("./views/cms/song/AddSong")
    },
    {
      path: "/cms/song/massadd",
      name: "cms-song-massadd",
      component: () => import("./views/cms/song/MassAddSong")
    },
    {
      path: "/cms/snippet",
      name: "cms-snippet-index",
      component: () => import("./views/cms/snippet/Index")
    },
    {
      path: "/cms/snippet/edit/:id",
      name: "cms-snippet-edit",
      component: () => import("./views/cms/snippet/EditSnippet")
    },
    {
      path: "/cms/snippet/add",
      name: "cms-snippet-add",
      component: () => import("./views/cms/snippet/AddSnippet")
    }
  ]
});

router.beforeEach((to, from, next) => {
  if (process.env.NODE_ENV !== "development" && to.name.includes("cms")) {
    return router.replace({ name: "SongsTab" });
  }

  return next();
});

export default router;
