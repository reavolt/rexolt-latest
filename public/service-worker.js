﻿importScripts("https://storage.googleapis.com/workbox-cdn/releases/4.3.0/workbox-sw.js");
if (workbox) {
  console.log(`Yay! Workbox is loaded 🎉`);
} else {
  console.log(`Boo! Workbox didn't load 😬`);
}
workbox.navigationPreload.enable();

workbox.routing.registerRoute(
  /\.(?:png|gif|jpg|jpeg|svg|webp)$/,
  new workbox.strategies.CacheFirst({
    cacheName: "images",
    plugins: [
      new workbox.expiration.Plugin({
        maxEntries: 60,
        maxAgeSeconds: 30 * 24 * 60 * 60 // 30 Days
      })
    ]
  })
);
workbox.routing.registerRoute(
  /\.(?:js|css)$/,
  new workbox.strategies.StaleWhileRevalidate({
    cacheName: "static-resources"
  })
);
workbox.routing.registerRoute(
  /.*(?:onesignal|algolia)\.com.*$/,
  new workbox.strategies.NetworkFirst({
    networkTimeoutSeconds: 3,
    cacheName: "data",
    plugins: [
      new workbox.expiration.Plugin({
        maxEntries: 50,
        maxAgeSeconds: 5 * 60 // 5 minutes
      })
    ]
  })
);
workbox.routing.registerRoute(
  new workbox.routing.NavigationRoute(new workbox.strategies.NetworkOnly())
);
self.addEventListener("fetch", function(e) {});
